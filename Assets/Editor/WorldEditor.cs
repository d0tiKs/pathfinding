﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(WorldGenerator))]
public class WorldEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        var world = (WorldGenerator)target;

        if(GUILayout.Button("Generate World"))
        {
            world.GenerateWorld();
        }

        if (GUILayout.Button("Destroy World"))
        {
            world.DestroyWorld();
        }
    }
}

