﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Pause : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    bool isPaused = false;

    const string PauseText = "Pause";
    const string ResumeText = "Resume";

    UIController UIController;

    private void Start()
    {
        UIController = GetComponentInParent<UIController>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if(isPaused)
        {
            GetComponentInChildren<Text>().text = PauseText;
            isPaused = false;
            UIController.Resume();
        }
        else
        {
            GetComponentInChildren<Text>().text = ResumeText;
            isPaused = true;
            UIController.Pause();
        }
    }
}
