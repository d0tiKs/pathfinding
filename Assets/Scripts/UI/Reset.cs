﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Reset : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        var agent = FindObjectOfType<Agent>();
        agent?.Reset();

        var world = FindObjectOfType<WorldGenerator>();
        world?.ResetFloor();
    }
}
