﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PathButton : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    PathFindingFactory.PathTypes Type;

    public void OnPointerClick(PointerEventData eventData)
    {
        var agent = FindObjectOfType<Agent>();
        agent.FollowPath(Type);
    }
}
