﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour {

    [SerializeField]
    WorldGenerator world;

    [SerializeField]
    Agent agent;

    float defaultTimeScale;

    private void Start()
    {
        defaultTimeScale = Time.timeScale;
    }

    public void Generate()
    {
        world.GenerateWorld();
    }

    public void ResetAgent()
    {
        
    }

    public void Pause()
    {
        Time.timeScale = 0f;
    }

    public void Resume()
    {
        Time.timeScale = defaultTimeScale;
    }


}
