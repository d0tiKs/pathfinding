﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Generate : MonoBehaviour, IPointerClickHandler
{
    UIController UIController;

    private void Start()
    {
        UIController = GetComponentInParent<UIController>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        UIController.Generate();
    }
}
