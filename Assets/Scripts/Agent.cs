﻿using System.Collections.Generic;
using UnityEngine;
using static PathFindingFactory;

public class Agent : MonoBehaviour
{
    [SerializeField]
    WorldGenerator world;

    [SerializeField]
    [Range(1, 20)]
    float Speed = 5;

    IReadOnlyList<IReadOnlyList<GameObject>> TileMap;
    Stack<Vector2> path;

    Vector2? currentPosition;
    Vector2? nextPosition;

    float distanceFraction;

    bool moving = false;

    void Start()
    {

        if (world == null)
            return;

        TileMap = world.TileMap;
    }

    public void Reset()
    {
        transform.position = world.Origin.transform.position;
        currentPosition = null;
        nextPosition = null;
        path = null;
        moving = false;
    }

    public void FollowPath(PathTypes type)
    {
        var pathFinding = world.Factory.CreatePathFinding(type);

        pathFinding.ComputePath();
        path = pathFinding.GetResult();
        distanceFraction = 0;
        moving = true;
    }

    private void Update()
    {
        if (moving)
            MoveToNextPosition();
    }

    void MoveToNextPosition()
    {
        if (path?.Count > 0)
        {
            if (currentPosition == null)
            {
                currentPosition = world.Origin.transform.position;
                nextPosition = path.Pop();
                transform.position = currentPosition.Value;
            }
            else if (distanceFraction >= 1)
            {
                distanceFraction = 0;
                currentPosition = nextPosition;
                nextPosition = path.Pop();
            }
        }
        else if(distanceFraction >= 1)
            moving = false;

        distanceFraction += Time.deltaTime * Speed;
        transform.position = Vector2.Lerp(currentPosition.Value, nextPosition.Value, distanceFraction);
    }

}
