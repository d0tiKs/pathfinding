﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Tile : MonoBehaviour
{
    public enum TileType
    {
        Start = 0,
        Floor = 1,
        Wall = 2,
        Goal = 3
    }

    [SerializeField]
    TileType type = TileType.Floor;

    public TileType Type
    {
        get { return type; }
        private set { type = value; }
    }

    public bool IsWalkable { get { return type != TileType.Wall; } }

    public void SetTypeAndTexure(TileType type, Sprite sprite)
    {
        this.type = type;
        GetComponent<SpriteRenderer>().sprite = sprite;
    }

}
