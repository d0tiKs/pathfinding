﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static Tile;

[Serializable]
public class TileTypeSpriteDictionary : SerializableDictionary<TileType, Sprite> { }

public class WorldGenerator : MonoBehaviour
{

    public GameObject Origin;
    public GameObject Goal;

    public List<List<GameObject>> TileMap
    {
        get { return tiles; }
    }

    PathFindingFactory factory;
    public PathFindingFactory Factory
    {
        get
        {
            if (factory == null)
                factory = new PathFindingFactory(this);
            return factory;
        }

        private set { factory = value; }
    }

    [SerializeField]
    GameObject TileModel = null;
    [SerializeField]
    GameObject MarkerModel = null;

    [SerializeField]
    [Range(5, 100)]
    int Width = 5;

    [SerializeField]
    [Range(5, 100)]
    int Height = 5;

    [SerializeField]
    [Range(0, 100)]
    int FloorRate = 70;

    [SerializeField]
    TileTypeSpriteDictionary TileTextures = new TileTypeSpriteDictionary();

    List<List<GameObject>> tiles = new List<List<GameObject>>();
    List<GameObject> markers = new List<GameObject>();

    System.Random rng = new System.Random();

    private void Start()
    {
        GenerateWorld();
    }

    public void GenerateWorld()
    {
        DestroyWorld();

        for (int i = 0; i < Width; i++)
        {
            tiles.Add(new List<GameObject>());
            for (int j = 0; j < Height; j++)
            {

                var tile = Instantiate(TileModel, transform);
                tiles[i].Add(tile);

                tile.transform.position = new Vector2(i, j);

                TileType type = TileType.Floor;

                if (i + j == 0)
                    type = TileType.Start;
                else if (i + j == Width + Height - 2)
                    type = TileType.Goal;
                else if (rng.Next(0, 100) > FloorRate)
                    type = TileType.Wall;

                tile.GetComponent<Tile>().SetTypeAndTexure(type, TileTextures[type]);
            }
        }

        SetupObjectives();

        MoveCamera();
    }

    void SetupObjectives()
    {
        Origin = TileMap[0][0];
        Goal = TileMap[Width - 1][Height - 1];
    }

    public void DestroyWorld()
    {
        foreach (var t in tiles.SelectMany(l => l))
            DestroyImmediate(t);

        tiles = new List<List<GameObject>>();
    }

    void MoveCamera()
    {
        var cameraPosition = new Vector3((float)Width / 2, (float)Height / 2 - TileModel.transform.localScale.y, -10);
        Camera.main.transform.position = cameraPosition;
    }

    public List<GameObject> WalkableNeighbours(GameObject node)
    {
        int x = (int)node.transform.position.x;
        int y = (int)node.transform.position.y;

        List<GameObject> neighbours = new List<GameObject>();

        bool minX = x > 0;
        bool maxX = x < Width - 1;

        bool minY = y > 0;
        bool maxY = y < Height - 1;


        if (minX && TileMap[x - 1][y].GetComponent<Tile>().IsWalkable)
            neighbours.Add(TileMap[x - 1][y]);

        if (minY && TileMap[x][y - 1].GetComponent<Tile>().IsWalkable)
            neighbours.Add(TileMap[x][y - 1]);

        if (maxX && TileMap[x + 1][y].GetComponent<Tile>().IsWalkable)
            neighbours.Add(TileMap[x + 1][y]);

        if (maxY && TileMap[x][y + 1].GetComponent<Tile>().IsWalkable)
            neighbours.Add(TileMap[x][y + 1]);

        return neighbours;
    }

    public void MarkFloor(Vector2 position)
    {
        var tile = TileMap[(int)position.x][(int)position.y];

        if (!tile.GetComponent<Tile>().IsWalkable)
            return;

        var marker = Instantiate(MarkerModel, transform);
        marker.transform.position = tile.transform.position;

        markers.Add(marker);
    }

    public void ResetFloor()
    {
        foreach (var marker in markers)
            Destroy(marker);

        markers.Clear();
    }
}
