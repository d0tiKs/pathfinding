﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class PathFindingFactory
{
    public enum PathTypes
    {
        BFS,
        DFS,
        Dijkstra,
        Astar
    }

    WorldGenerator world;

    public PathFindingFactory(WorldGenerator world)
    {
        this.world = world;
    }

    public IPathFinding CreatePathFinding(PathTypes type)
    {
        switch (type)
        {
            case PathTypes.BFS:
                return new BFS(world);
            case PathTypes.DFS:
                throw new NotImplementedException();
            case PathTypes.Dijkstra:
                throw new NotImplementedException();
            case PathTypes.Astar:
                throw new NotImplementedException();
        }

        return null;
    }


}
