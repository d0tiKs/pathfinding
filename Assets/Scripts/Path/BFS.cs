﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;

public class BFS : IPathFinding
{
    IReadOnlyList<IReadOnlyList<GameObject>> tileMap;
    Dictionary<GameObject, GameObject> nodesParent = new Dictionary<GameObject, GameObject>();

    WorldGenerator world;

    bool succeeded = false;

    public BFS(WorldGenerator world)
    {
        this.world = world;
    }

    public bool ComputePath()
    {
        var goal = world.Goal;
        var origin = world.Origin;

        nodesParent.Clear();

        Queue<GameObject> queue = new Queue<GameObject>();

        queue.Enqueue(origin);
        int i = 0;
        while (queue.Count > 0)
        {
            var currentNode = queue.Dequeue();

            if (currentNode == goal)
            {
                succeeded = true;
                return succeeded;
            }

            List<GameObject> neighbours = world.WalkableNeighbours(currentNode);

            Debug.Log(i++);
            foreach (var neighbour in neighbours)
            {
                if (nodesParent.ContainsKey(neighbour))
                    continue;

                nodesParent.Add(neighbour, currentNode);
                queue.Enqueue(neighbour);
            }
        }
        var debug = nodesParent.Keys.FirstOrDefault(k => k.transform.position.x == 14 && k.transform.position.y == 14);
        return succeeded;
    }

    public Stack<Vector2> GetResult()
    {
        if (!succeeded)
            return null;

        Stack<Vector2> result = new Stack<Vector2>();
        var node = world.Goal;

        do
        {
            result.Push(node.transform.position);
            world.MarkFloor(node.transform.position);
            node = nodesParent[node];
        } while (node != world.Origin);


        return result;
    }
}
